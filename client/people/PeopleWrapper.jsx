import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import PeopleForm from './PeopleForm.jsx';
import Person from './Person.jsx';

People = new Mongo.Collection("people");

export default class PeopleWrapper extends TrackerReact(React.Component) {
	constructor() {
		super();

		this.state = {
			subscription: {
				people: Meteor.subscribe("userPeople")
			}
		}
	}

	componentWillUnmount() {
		this.state.subscription.people.stop();
	}

	people() {
		return People.find().fetch();
	}

	render() {
		return (
			<ReactCSSTransitionGroup
				component="div"
				transitionName="route"
				transitionEnterTimeout={600}
				transitionAppearTimeout={600}
				transitionLeaveTimeout={400}
				transitionAppear={true}>
				<h1>People</h1>
				<PeopleForm />
				<ReactCSSTransitionGroup
					component="ul"
					className="people"
					transitionName="personLoad"
					transitionEnterTimeout={600}
					transitionLeaveTimeout={400}>
					{this.people().map( (person)=>{
						return <Person key={person._id} person={person} />
					})}
				</ReactCSSTransitionGroup>
			</ReactCSSTransitionGroup>
		)
	}
}
