import React, {Component} from 'react';

export default class Person extends Component {

	toggleChecked() {
		Meteor.call('togglePerson', this.props.person);
	}

	deletePerson() {
		Meteor.call('deletePerson', this.props.person);
	}

	render() {
		const personClass = this.props.person.vip ? "checked" : "";
		const status = this.props.person.vip ? <span className="vipp">vip</span> : '';

		return (
			<li className={personClass}>
				<input type="checkbox"
					readOnly={true}
					checked={this.props.person.vip}
					onClick={this.toggleChecked.bind(this)} />
				{this.props.person.text}
				{status}
				<button className="btn-cancel"
					onClick={this.deletePerson.bind(this)}>
					&times;
				</button>
			</li>
		)
	}
}
