import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class PersonDetail extends TrackerReact(Component) {
	constructor() {
		super();
		this.state = {
			subscription: {
				people: Meteor.subscribe("userPeople")
			}
		}
	}

	componentWillUnmount() {
		this.state.subscription.people.stop();
	}

	people() {
		return People.findOne(this.props.id);
	}

	render() {
		let res = this.people();

		if(!res) {
			return(<div>Loading....</div>);
		}

		return (
			<div>
				<h1>{res.text}</h1>
			</div>
		)
	}
}
