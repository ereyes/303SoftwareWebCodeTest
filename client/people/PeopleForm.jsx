import React, {Component} from 'react';

export default class PeopleForm extends Component {

	importPeople(event) {
		event.preventDefault();
		var url = "http://www.filltext.com/?rows=100&fname=%7BfirstName%7D&lname=%7BlastName%7D&city=%7Bcity%7D&pretty=true";
		if(url){
			Meteor.call('importPeopleFromURL', url, (error, data)=> {
				if(error) {
					Bert.alert('Please login before importing', 'danger', 'fixed-top', 'fa-frown-o' );
				}
			});
		}
	}

	removeAllPeople(event) {
		event.preventDefault();
		Meteor.call('removeAllPeople', (error, data)=> {
			if(error) {
				Bert.alert('Please login before removing', 'danger', 'fixed-top', 'fa-frown-o' );
			}
		});
	}

	addPerson(event) {
		event.preventDefault();
		var text = this.refs.person.value.trim();
		if(text){
			Meteor.call('addPerson', text, (error, data)=> {
				if(error) {
					Bert.alert('Please login before submitting', 'danger', 'fixed-top', 'fa-frown-o' );
				} else {
					this.refs.person.value = "";
				}
			});
		}
	}

	render() {
		return (
			<div>
			<button className="btn-confirmation"
				onClick={this.importPeople}>
				Import People
			</button>
			<button className="btn-remove"
				onClick={this.removeAllPeople}>
				Remove all People
			</button>
				<form className="new-person" onSubmit={this.addPerson.bind(this)}>
					<input
						type="text"
						ref="person"
						placeholder="Just Full Name in this v. I am still working... :)" />
				</form>
			</div>
		)
	}
}
