import React, {Component} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

export default class About extends Component {

	setVar() {
		Session.set('Meteor.loginButtons.dropdownVisible', true);
	}

	render() {
		return (
			<ReactCSSTransitionGroup
				component="div"
				transitionName="route"
				transitionEnterTimeout={600}
				transitionAppearTimeout={600}
				transitionLeaveTimeout={400}
				transitionAppear={true}>
				<p>My solution for:</p>
				<p>https://gist.github.com/fostahgix/c280ed72c61a3edc8d9d5a2b8cafd217</p>

				<h1>About Me:</h1>
				<h1>Eduardo Rodríguez Reyes</h1>
				<h2>ed.rodriguez97@gmail.com</h2>
				<h2>movil: +52 998 389 1733 (Telegram, Whatsapp, Skype)</h2>
				<h2>SUMMARY:</h2>
				<p>
				I am IT Engineer living in Cancun Mexico with more than eleven years of experience developing full-stack Nodejs, Javascript, Meteor, Blaze, React and Angular
and Java applications. Excellent organizational, analytical and problem solving skills. I am a team player and
an innovative thinker who seeks functional solutions by applying strong object oriented development skills
and software engineer patterns. Proficient in Javascript, Java, PHP, .NET HTML5, CSS3, JQuery, JSON, SQL,
MongoDB, PostgreSQL, MySQL, Microsoft SQL Server, GIS, Data Warehousing, Business Inteligence,
using tools like WebStorm, Visual Studio Code, Eclipse and Qgis. Daily routine with teamwork tools like
Git.
				</p>				
			</ReactCSSTransitionGroup>
		)
	}
}
