import React from 'react';
import {mount} from 'react-mounter';

import {MainLayout} from './layouts/MainLayout.jsx';
import PeopleWrapper from './people/PeopleWrapper.jsx';
import About from './About.jsx';
import PersonDetail from './people/PersonDetail.jsx';


FlowRouter.route('/', {
	action() {
		mount(MainLayout, {
			content: (<PeopleWrapper />)
		})
	}
});

FlowRouter.route('/about', {
	action() {
		mount(MainLayout, {
			content: (<About />)
		})
	}
});

FlowRouter.route('/people/:id', {
	action(params) {
		mount(MainLayout, {
			content: (<PeopleDetail id={params.id} />)
		})
	}
});
