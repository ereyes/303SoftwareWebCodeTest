People = new Mongo.Collection("people")



Meteor.publish("allPeople", function(){
	return People.find()
})

Meteor.publish("userPeople", function(){
	return People.find({user: this.userId})
})
